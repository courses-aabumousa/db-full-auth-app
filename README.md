create basic app 


1. npm install prisma --save-dev

2. npx prisma init --datasource-provider mysql

3. setup connection string in .env file
for example
DATABASE_URL="mysql://root:JuneCourse@2023@localhost:3306/june_course"


4. write first model, for example

model Task {
  id Int @id @default(autoincrement())
  name String
  priority Int
}

5. create migration "this should be done to commit db changes"
 npx prisma migrate dev --name init

6. generate the prisma client
npx prisma generate

7.setup the client wherever needed

8. wrap endpoints with try/catch, instead of throwing async exceptions replace with moving the error with next(error)

https://www.prisma.io/docs/reference/api-reference/error-reference

9. use prisma client to access and apply db operations


---

update the app to use relationship

1. modify the schema as needed
ex.
model Task {
  id Int @id @default(autoincrement())
  name String
  priority Int
  completed  Boolean @default(false)
  user  User @relation(fields: [userId], references: [id])
  userId Int
}

model User {
  id Int @id @default(autoincrement())
  username String @unique
  password String
  tasks Task[]
}

2. run the migration

---

authentication

1. hash the passwords on store (not recommended to store plain text passwords)
you can use bcrypt
npm i bcrypt
then on create/update user make sure to hash the password
import bcrypt from 'bcrypt';
export const roundsOfHashing = 10;

hash user password
const hashedPassword = await bcrypt.hash(
  password,
  roundsOfHashing,
);
  

verify user password
const isValidPassword = await bcrypt.compare(password, user.password)


2. install jsonwebtoken package
npm i jsonwebtoken

3. setup generate and verify token functions 
export const generateToken = (id, username) => {
  jwt.sign(
    {
      id,
      username,
    },
    secret
  );
};

export const verifyToken = (token) => {
    return jwt.verify(token, secret, (error, user) => {
        if (error) throw error;
        return user;
    })
}