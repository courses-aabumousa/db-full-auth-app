import { Router } from "express";
import { body, param } from "express-validator";
import createHttpError from "http-errors";
import { validateResultMiddleware } from "./validateResultMiddleware.js";
import { PrismaClient } from "@prisma/client";
import bcrypt, { hash } from 'bcrypt';
import { generateToken } from "./auth.js";
export const roundsOfHashing = 10;

const prisma = new PrismaClient();

const usersRouter = Router();

usersRouter.post(
  "/auth",
  [
    body("username").exists().isString(),
    body("password").exists().isString()
  ],
  validateResultMiddleware,
  async (req, res, next) => {
    try {
      const { username, password } = req.body;
    
  
      const user = await prisma.user.findFirstOrThrow({
        where: {
          username
        }
      });
      
      const isValidPassword = await bcrypt.compare(password, user.password)
      if (!isValidPassword) {
        next(createHttpError.Unauthorized('invalid password'))
      }
      const generatedToken = generateToken(user.id, user.username);
      res.status(200).json({
        token: generatedToken
      });
    } catch (error) {
      next(createHttpError.InternalServerError(error));
    }
  }
);


usersRouter.post(
  "",
  [
    body("username").exists().isString(),
    body("password").exists().isString()
  ],
  validateResultMiddleware,
  async (req, res) => {
    try {
      const { username, password } = req.body;
      const hashedPassword = await bcrypt.hash(
        password,
        roundsOfHashing,
      );
  
      const newUser = await prisma.user.create({
        data: {
            username,
            password: hashedPassword,
        },
      });

      res.status(201).json(newUser);
    } catch (error) {
      next(createHttpError.InternalServerError(error));
    }
  }
);

// PUT /users/:id endpoint
usersRouter.put(
  "/:id",
  [
    param("id").isInt(),
    body("username").exists().isString(),
    body("password").exists().isString(),
  ],
  validateResultMiddleware,
  async (req, res, next) => {
    try {
      const userId = parseInt(req.params.id);
      const { username, password } = req.body;
      const hashedPassword = await bcrypt.hash(
        password,
        roundsOfHashing,
      );
  
      const userToUpdate = await prisma.user.update({
        where: { id: userId },
        data: {
            username,
            password: hashedPassword,
        },
      });

      res.status(200).json(userToUpdate);
    } catch (error) {
      if (error.code === "P2025") {
        next(createHttpError.NotFound(error));
      }
      next(createHttpError.InternalServerError(error));
    }
  }
);

// GET /users endpoint
usersRouter.get("", async (req, res) => {
  try {
    const users = await prisma.user.findMany();
    res.json(users);
  } catch (error) {
    next(createHttpError.InternalServerError(error));
  }
});

// GET /users/:id endpoint
usersRouter.get("/:id", async (req, res, next) => {
  try {
    const userId = parseInt(req.params.id);
    const user = await prisma.user.findUnique({
        where: {
            id: userId
        }, include: {
            tasks: {
                where: {
                    priority: {
                        equals: 5
                    }
                }
            },
            _count: {
                select: {
                    tasks: true
                }
            }
          }
    })


    res.json(user);
  } catch (error) {
    if (error.code === "P2025") {
      next(createHttpError.NotFound(error));
    }
    next(createHttpError.InternalServerError(error));
  }
});

// DELETE /users/:id endpoint
usersRouter.delete("/:id", async (req, res, next) => {
  try {
    const userId = parseInt(req.params.id);

    const deletedUser = await prisma.user.delete({
      where: {
        id: userId
      }
    })

    res.json(deletedUser);
  } catch (error) {
    if (error.code === "P2025") {
      next(createHttpError.NotFound(error));
    }
    next(createHttpError.InternalServerError(error));
  }
});

export default usersRouter;
