import { Router } from "express";
import { body, param, query } from "express-validator";
import createHttpError from "http-errors";
import { validateResultMiddleware } from "./validateResultMiddleware.js";
import { PrismaClient } from "@prisma/client";
import { authenticationMiddleware } from "./auth.js";

const prisma = new PrismaClient();

const tasksRouter = Router();

tasksRouter.post(
  "",
  authenticationMiddleware,
  [
    body("name").exists().isString(),
    body("priority").exists().isInt({ min: 1, max: 5 }),
  ],
  validateResultMiddleware,
  async (req, res) => {
    try {
      const { name, priority } = req.body;
      const user = req.user;
      const newTask = await prisma.task.create({
        data: {
          name,
          priority,
          userId: user.id,
        },
      });

      res.status(201).json(newTask);
    } catch (error) {
      next(createHttpError.InternalServerError(error));
    }
  }
);

// PUT /tasks/:id endpoint
tasksRouter.put(
  "/:id",
  [
    param("id").isInt(),
    body("name").exists().isString(),
    body("priority").exists().isInt({ min: 1, max: 5 }),
    body("userId").exists().isInt(),
  ],
  validateResultMiddleware,
  async (req, res, next) => {
    try {
      const taskId = parseInt(req.params.id);
      const { name, priority, userId } = req.body;

      const taskToUpdate = await prisma.task.update({
        where: { id: taskId },
        data: {
          name,
          priority,
          userId,
        },
      });

      res.status(200).json(taskToUpdate);
    } catch (error) {
      if (error.code === "P2025") {
        next(createHttpError.NotFound(error));
      }
      next(createHttpError.InternalServerError(error));
    }
  }
);

// GET /tasks endpoint
tasksRouter.get("",[
  query('userId').isInt().toInt(),
  query('priority').isInt().toInt(),
], validateResultMiddleware,
 async (req, res, next) => {
  try {
    const filters = req.query;

    const tasks = await prisma.task.findMany({ where: filters });
    res.json(tasks);
  } catch (error) {
    next(createHttpError.InternalServerError(error));
  }
});

// GET /tasks/:id endpoint
tasksRouter.get("/:id", async (req, res, next) => {
  try {
    const taskId = parseInt(req.params.id);

    const task = await prisma.task.findFirstOrThrow({
      where: {
        id: taskId,
      },
    });

    res.json(task);
  } catch (error) {
    if (error.code === "P2025") {
      next(createHttpError.NotFound(error));
    }
    next(createHttpError.InternalServerError(error));
  }
});

// DELETE /tasks/:id endpoint
tasksRouter.delete("/:id", async (req, res, next) => {
  try {
    const taskId = parseInt(req.params.id);

    // Remove the task from the tasks array
    const deletedTask = await prisma.task.delete({
      where: {
        id: taskId,
      },
    });

    res.json(deletedTask);
  } catch (error) {
    if (error.code === "P2025") {
      next(createHttpError.NotFound(error));
    }
    next(createHttpError.InternalServerError(error));
  }
});

export default tasksRouter;
